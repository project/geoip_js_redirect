/**
 * @file
 * Custom JS for the Geo IP Redirect module.
 */

(function ($) {
  'use strict';

Drupal.behaviors.geoIpRedirect = {
  attach: function (context, settings) {
    // Confirm that the settings are available.
    if (typeof settings.geoip_js_redirect === 'undefined') {
      return;
    }
    if (typeof settings.geoip_js_redirect.destinations === 'undefined') {
      return;
    }

    var cookie_value = $.cookie('geoip_js_redirect');

    // If the cookie wasn't set before, load the popup.
    if (cookie_value === null) {
      // Set the cookie so the page won't be loaded again.
      $.cookie('geoip_js_redirect', 'Not today, thank you.');

      // Try loading the country info from FreeGeoIP.net.
      jQuery.get(settings.geoip_js_redirect.service, function(response) {
        if (typeof response.country_code !== 'undefined') {
          var destinations = settings.geoip_js_redirect.destinations;
          for (var x = 0; x < destinations.length; ++x) {
            if (response.country_code === destinations[x].from) {
              window.location.href = destinations[x].to;
            }
          }
        }
      }, 'jsonp');
    }
  }
};

})(jQuery);
