GeoIP JS Redirect
-----------------
This module allows you to redirect visitors based upon the country they are
visiting from. It uses the freegeoip.net service to detect the visitor's
location, and then various options may be used to redirect the visitor to
another page.


Configuration
--------------------------------------------------------------------------------
Currently configuration is handled via a variable that must fit a specific
format that is most easily added to the site's settings.php file. There are
three specific details that must be identified:

* The path that should be checked. Only public pages will be matched, admin
  pages will not be checked. This value follows standard Block visibility rules,
  i.e.:
  * - every page will be matched.
  <front> - will match the site's front page.
  node/42 - will match node 42.
  blog - will match the "blog" page or a page alias.
  blog/* - will match all pages with an alias starting with "blog/".

* The country code to redirect from. This should be in uppercase, e.g. 'US',
  'JP', 'FR', etc.

* The destination should be an absolute path, or at least one that starts from
  the site's root path.


Examples
--------------------------------------------------------------------------------
This example redirects page requests to the node/4 page from visitors in Japan
to the /jp page:

$conf['geoip_js_redirect_destinations'] = array(
  // The internal path to pages that will be redirected from.
  'node/4' => array(
    // Nested array of values that will be looped over.
    array(
      // If the country code is this..
      'from' => 'JP',
      // .. the visitor will be redirected to here.
      'to' => '/jp',
    ),
  ),
);

Multiple items may be added for different paths, and different pairs of country
/ destination redirection for each, e.g.:

$conf['geoip_js_redirect_destinations'] = array(
  // This affects every page in the site.
  '*' => array(
    // Redirect visitors from Japan to the Japanese locale-aware homepage.
    array(
      'from' => 'JP',
      'to' => '/jp',
    ),
    // Redirect visitors from Spain to the Spain landing page.
    array(
      'from' => 'ES',
      'to' => '/spain',
    ),
  ),
  // These redirects only affect one specific node.
  'node/42' => array(
    // Redirect visitors from Spain to the Spain landing page.
    array(
      'from' => 'FR',
      'to' => '/france',
    ),
  ),
);


Credits / contact
--------------------------------------------------------------------------------
Currently maintained by Damien McKenna [1].

Ongoing development is sponsored by Mediacurrent [2].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/geoip_js_redirect


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
2: http://www.mediacurrent.com/
